const sqlite3 = require('sqlite3').verbose();
const request = require('request');

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }
 
// open the database
let db = new sqlite3.Database('/home/pintaf/Documentos/nextcloud/alacrite/customsbridg/EBTI data/EBTI.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the database.');
});
 

db.serialize(() => {
  db.each(`select BTI_REF, IMAGES from EBTI_CONSOLIDATED where is not null`, (err, row) => {
    if (err) {
      console.error(err.message);
    } else {
      row.IMAGES.split(';').forEach(function(URL) {
        var requestSettings = {
            method: 'GET',
            url: URL,
            encoding: null
        };
        request(requestSettings, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                db.run("insert into IMAGES VALUES(?,?,?)", row.BTI_REF, URL.slice(67+row.BTI_REF.length), body);
            }
        })
        sleep(1000); // In order to not be too hard on the server, we request once every second
      });

    }
    
  });
});

/*
sleep(60000); //Waiting one minute before closing the connection
db.close((err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Close the database connection.');
});*/
